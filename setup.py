#! /usr/bin/env python

import os

from distutils.core import setup

from peeq4 import appname, __version__

setup(
    name=appname,
    description="A Python editing environment for Qt",
    author="David Boddie",
    author_email="david@boddie.org.uk",
    url="http://www.boddie.org.uk/david/Projects/Python/Qt",
    version=__version__,
    packages = ["PEEQ4"],
    scripts = ["peeq4.py", "peeq4-child.py"]
    )
