#!/usr/bin/env python

"""
peeq4.py - a Python editing environment for PyQt4

Copyright (C) 2004-2010 David Boddie

This file is part of PEEQ4.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""

import imp, os, string, sys, traceback, types

from PyQt4.QtCore import QEvent, QObject, QPoint, QProcess, QStringList, Qt, \
                         SIGNAL, SLOT
from PyQt4.QtGui import QAction, QApplication, QBrush, QColor, QComboBox, \
                        QDialog, QFileDialog, QFont, QFontDatabase, \
                        QHBoxLayout, QInputDialog, \
                        QKeySequence, QLineEdit, QListView, QMainWindow, \
                        QMessageBox, QMenu, QPushButton, QStatusBar, \
                        QTextBrowser, QTextBlockFormat, QTextCharFormat, \
                        QTextCursor, \
                        QTextEdit, QTextOption, \
                        QVBoxLayout, QWidget, qApp

from PyQt4 import pyqtconfig

from PEEQ4.editors import Editor
from PEEQ4.formatters import encode
from PEEQ4.handlers import StreamHandler, StreamReader
from PEEQ4.iobuffer import Buffer, History


# Define the application details.

appname = "peeq4"
__version__ = "0.30"


# Define a start up message for the interpreter.

start_up = \
"""<h1 class = "Title">%s %s</h1>

<p>
  <em>python:</em>
  Type "help", "copyright", "credits" or "license" for more information.
</p>
<p>
  <em>%s:</em>
  Type "peeq4_license" for license information specific to this environment.
</p>
""" % (appname, __version__, appname)


class GUIInterface(QMainWindow):

    """GUIInterface(QMainWindow)
    
    This class provides an interface to child processes that execute the
    code.
    
    gui = GUIInterface(application, environment)
    
    The GUIInterface object is a subclass of QObject in order to allow
    executed Python code to access Qt classes.
    """
    
    display_width = 80
    editor_keys = (Qt.Key_Left, Qt.Key_Right, Qt.Key_Up, Qt.Key_Down,
                   Qt.Key_Backspace, Qt.Key_Delete, Qt.Key_Home, Qt.Key_End)
    
    # Redefine the tr() function for this class.
    def tr(self, text):
    
        return qApp.translate("GUIInterface", text)
    
    def __init__(self, app):
    
        QMainWindow.__init__(self)
        
        self.app = app
        config = pyqtconfig.Configuration()
        
        try:
            self.qtdir = config.qt_dir
        except AttributeError:
            self.qtdir = os.getenv("QTDIR")
        
        if self.qtdir:
            self.documents_path = os.path.join(self.qtdir, "doc", "html")
        else:
            self.documents_path = None
        
        central_widget = QWidget(self)
        
        left_frame = QWidget(central_widget)
        
        self.text_browser = QTextBrowser(left_frame)
        self.text_browser.setFocusPolicy(Qt.NoFocus)
        self.text_browser.setWordWrapMode(QTextOption.WrapAtWordBoundaryOrAnywhere)
        #self.text_browser.setTextFormat(QTextEdit.RichText)
        
        self.combo_edit = QComboBox(left_frame)
        self.combo_edit.setAutoCompletion(1)
        self.combo_edit.setEditable(1)
        self.combo_edit.installEventFilter(self)
        self.combo_edit.lineEdit().installEventFilter(self)
        self.combo_edit.setDuplicatesEnabled(0)
        
        # Set up the child process, user interface and document appearance.
        self.setup_child_process()
        self.setup_menus()
        self.setup_text_styles()
        
        self.connect(
            self.combo_edit, SIGNAL("activated(const QString &)"),
            self.combo_edit, SLOT("setEditText(const QString &)")
            )
        
        left_layout = QVBoxLayout(left_frame)
        left_layout.addWidget(self.text_browser, 1)
        left_layout.addWidget(self.combo_edit, 0)
        left_layout.setMargin(0)
        
        layout = QHBoxLayout(central_widget)
        layout.addWidget(left_frame, 5)
        
        self.setCentralWidget(central_widget)
        
        # Print a welcome message.
        self.text_browser.append(start_up)
        self.add_output(u"\n", self.base_format)
    
    def setup_child_process(self):
    
        # Create an environment to run Python scripts in.
        # After this, the self.environment object will be available for use.
        # This contains the module object which can have objects added to it.
        self.child_process = QProcess()
        
        self.connect(self.child_process, SIGNAL("readyReadStandardOutput()"),
                     self.handle_stdout)
        self.connect(self.child_process, SIGNAL("readyReadStandardError()"),
                     self.handle_stderr)
        self.connect(self.child_process, SIGNAL("stateChanged(QProcess::ProcessState)"),
                     self.state_changed)
        self.connect(self.child_process, SIGNAL("error(QProcess::ProcessError)"),
                     self.handle_error)
        self.connect(self.child_process, SIGNAL("finished(int, QProcess::ExitStatus)"),
                     self.handle_exit)
        
        self.stdout_parser = StreamReader(
            {"output": self.write_stdout,
             "matches": self.show_completions,
             "match": self.handle_completion_match,
             "index": self.handle_completion_index}
            )
        self.parsing_stdout = False
        
        self.stderr_parser = StreamReader(
            {"output": self.write_stderr}
            )
        
        self.parsing_stderr = False
        
        arguments = QStringList()
        this_dir = os.path.split(os.path.abspath(__file__))[0]
        arguments << "-u"
        arguments << os.path.join(this_dir, "peeq4-child.py")
        self.child_process.start("python", arguments)
        
        xml_input = u"<session>\n"
        self.child_process.write(xml_input.encode("utf-8"))
        
        # Create a history of input strings and a variable to contain the
        # currently edited text.
        self.history = History()
        self.current_text = ""
        
        # Keep a record of the current completion matches and starting index.
        self.completion_matches = []
        self.completion_index = 0
        
        # Create a list of editors.
        self.editors = []
        
    def setup_menus(self):
    
        # File menu
        self.file_menu = QMenu(self.tr("&File"), self)
        exit_action = self.file_menu.addAction(self.tr("E&xit"))
        exit_action.setShortcut(QKeySequence(self.tr("Ctrl+Q")))
        self.connect(exit_action, SIGNAL("triggered(bool)"), self, SLOT("close()"))
        
        # Environment menu
        self.child_process_menu = QMenu(self.tr("&Environment"), self)
        #self.child_process_menu.setCheckable(1)
        
        editor_action = self.child_process_menu.addAction(self.tr("Create edi&tor"))
        editor_action.setShortcut(QKeySequence(self.tr("Ctrl+Shift+E")))
        
        self.connect(editor_action, SIGNAL("triggered(bool)"), self.create_editor)
        
        # History menu (dynamically generated)
        self.history_menu = QMenu(self.tr("H&istory"), self)
        
        # Help menu
        self.help_menu = QMenu(self.tr("&Help"), self)
        about_action = self.help_menu.addAction(self.tr("&About %1").arg(appname))
        
        self.connect(about_action, SIGNAL("triggered(bool)"), self.show_help)
        self.connect(self.history_menu, SIGNAL("aboutToShow()"), self.prepare_history)
        self.connect(self.history_menu, SIGNAL("triggered(QAction*)"), self.select_history)
        
        # Add the menus to the menu bar.
        self.menuBar().addMenu(self.file_menu)
        self.menuBar().addMenu(self.child_process_menu)
        self.menuBar().addMenu(self.history_menu)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)
    
    def setup_text_styles(self):
    
        fonts = QFontDatabase()
        use_font = "Monospace"
        for font in fonts.families():
        
            if fonts.isFixedPitch(font):
                use_font = font
                break
        
        self.base_format = QTextCharFormat()
        self.base_format.setFontFamily(use_font)
        
        self.input_format = QTextCharFormat(self.base_format)
        self.input_format.setFontItalic(False)
        self.input_format.setForeground(QBrush(Qt.blue))
        
        self.output_format = QTextCharFormat(self.base_format)
        self.output_format.setFontItalic(False)
        self.output_format.setForeground(QBrush(QColor(0, 160, 0)))
        
        self.error_format = QTextCharFormat(self.base_format)
        self.error_format.setFontItalic(False)
        self.error_format.setForeground(QBrush(Qt.red))
        self.error_format.setFontWeight(QFont.Bold)
        
        self.block_format = QTextBlockFormat()
    
    def eventFilter(self, obj, event):
    
        """eventFilter(self, event)
        
        Monitor the events from the line edit widget.
        """
        
        if obj == self.combo_edit:
        
            if event.type() == QEvent.KeyPress:
            
                line_edit = self.combo_edit.lineEdit()
                if event.key() == Qt.Key_Tab:
                
                    self.complete_text(
                        str(line_edit.text()), line_edit.cursorPosition()
                        )
                    return True
                
                elif event.key() == Qt.Key_Return:
                
                    self.execute_text(line_edit.text())
                    self.combo_edit.clear()
                    return True
                
                elif event.key() == Qt.Key_Up:
                
                    obj.lineEdit().setText(self.history.previous(str(obj.lineEdit().text())))
                    return True
                
                elif event.key() == Qt.Key_Down:
                
                    obj.lineEdit().setText(self.history.next(str(obj.lineEdit().text())))
                    return True
                
                else:
                
                    self.combo_edit.hidePopup()
        
        elif event.type() == QEvent.KeyPress:
        
            listView = self.combo_edit.findChild(QListView)
            
            if obj == listView:
            
                if event.key() == Qt.Key_Backspace:
                
                    current_text = self.combo_edit.lineEdit().text()
                    current_text = current_text.left(current_text.length() - 1)
                    self.combo_edit.lineEdit().setText(current_text)
                    
                    self.complete_text(
                        str(current_text),
                        self.combo_edit.lineEdit().cursorPosition()
                        )
                    
                    return True
                
                elif event.key() == Qt.Key_Tab:
                
                    self.combo_edit.hidePopup()
                    return True
                
                elif event.key() == Qt.Key_Return:
                
                    selection = listView.selectionModel().currentIndex()
                    text = listView.model().data(selection).toString()
                    self.combo_edit.hidePopup()
                    self.combo_edit.clear()
                    self.combo_edit.lineEdit().setText(text)
                    return True
                
                elif event.key() == Qt.Key_Up:
                
                    pass
                
                elif event.key() == Qt.Key_Down:
                
                    pass
                
                else:
                
                    print self.combo_edit.lineEdit().cursorPosition()
                    current_text = self.combo_edit.lineEdit().text()
                    current_text.append(event.text())
                    self.combo_edit.lineEdit().setText(current_text)
                    
                    self.complete_text(
                        str(current_text),
                        self.combo_edit.lineEdit().cursorPosition()
                        )
                    
                    return True
        
        return QMainWindow.eventFilter(self, obj, event)
    
    def closeEvent(self, event):
    
        event.accept()
        for editor in self.editors:
            editor.close()
        
        if self.child_process.state() == QProcess.Running:
        
            # Send the exit command to the child process.
            xml_input = u"<exit /></session>\n"
            self.child_process.write(xml_input.encode("utf-8"))
            self.child_process.waitForFinished(2000)
    
    def create_editor(self):
    
        """Show an editor for Python code."""
        widget = Editor(self.base_format)
        self.connect(widget, SIGNAL("executeCode(const QString &, bool)"),
                     self.execute_text)
        self.editors.append(widget)
        widget.show()
    
    def prepare_history(self):
    
        self.history_menu.clear()
        
        current, items = self.history.items()
        for i in range(len(items)):
        
            item = items[i]
            item_action = self.history_menu.addAction(item)
            
            if current == i:
                item_action.setChecked(True)
    
    def select_history(self, index):
    
        self.combo_edit.lineEdit().setText(index.text())
    
    def show_help(self):
    
        QMessageBox.about(
            self, self.tr("About %1").arg(appname),
            self.tr(
                "<p><b>An interactive editing environment for PyQt4.</b>\n\n"
                "<p>Copyright (C) 2006, David Boddie\n\n"
                "<p>Type peeq4_license for license information."
                )
            )
    
    # Utility methods
    
    def execute_text(self, text, display = True):
    
        # Examine the text submitted.
        display_text = text = unicode(text) + u"\n"
        
        if display:
            # Echo the text submitted to the output window.
            self.add_output(display_text, self.input_format)
        
        # Send the user's input to the child process for execution.
        xml_input = self.encode_input("input", text)
        self.child_process.write(xml_input.encode("utf-8"))
        
        # Clear the line edit and save the command in the command history.
        if not self.combo_edit.lineEdit().text().isEmpty():
            self.history.append(self.combo_edit.lineEdit().text())
            self.combo_edit.lineEdit().clear()
    
    def handle_stdout(self):
    
        data = self.child_process.readAllStandardOutput()
        self.stdout_parser.source.setData(data)
        
        if not self.parsing_stdout:
            self.stdout_parser.parse()
            self.parsing_stdout = True
        else:
            self.stdout_parser.parseContinue()
    
    def handle_stderr(self):
    
        data = self.child_process.readAllStandardError()
        self.stderr_parser.source.setData(data)
        
        if not self.parsing_stderr:
            self.stderr_parser.parse()
            self.parsing_stderr = True
        else:
            self.stderr_parser.parseContinue()
    
    def write_stdout(self, text):
    
        data = unicode(text)
        output = self.wrap_text(data)
        
        # Use the appropriate formatter to present the value
        # obtained.
        
        self.add_output(output, self.output_format)
    
    def write_stderr(self, text):
    
        data = unicode(text)
        output = self.wrap_text(data)
        
        # Use the appropriate formatter to present the value
        # obtained.
        
        self.add_output(output, self.error_format)
    
    def state_changed(self, state):
    
        print "state changed:", state
    
    def handle_error(self, error):
    
        print "error:", error
    
    def handle_exit(self, code, status):
    
        print "exit:", code, status
    
    def handle_completion_index(self, text):
    
        try:
            self.completion_index = int(text)
        except ValueError:
            self.completion_index = 0
    
    def handle_completion_match(self, text):
    
        self.completion_matches.append(text)
    
    def complete_text(self, text, cursor):
    
        text_element = self.encode_input("text", text)
        cursor_element = "<cursor>%i</cursor>" % cursor
        data = u"<complete>\n"+text_element+cursor_element+u"</complete>\n"
        
        self.child_process.write(data.encode("utf-8"))
    
    def show_completions(self, data):
    
        matches = self.completion_matches
        start_at = self.completion_index
        self.completion_matches = []
        self.completion_index = 0
        #print text, cursor, start_at, matches
        
        if not matches:
            return
        
        text = unicode(self.combo_edit.lineEdit().text())
        cursor = self.combo_edit.lineEdit().cursorPosition()
        self.combo_edit.clear()
        
        next = min(filter(
            lambda index: index != -1,
            map(lambda c: text.find(c, cursor), "(),. ") + [len(text)]
            ))
        
        if next == len(text):
            rest = ""
        else:
            rest = text[next:]
        
        if len(matches) == 1:
        
            self.combo_edit.lineEdit().setText(text[:start_at]+matches[0]+rest)
            self.combo_edit.lineEdit().setCursorPosition(
                start_at + len(matches[0])
                )
        
        elif len(matches) > 1:
        
            # Insert the item back into the list so that the line edit's text
            # doesn't get overwritten.
            self.combo_edit.addItem(text)
            
            for match in matches:
            
                # Insert the combined text for each match into the combo box.
                self.combo_edit.addItem(text[:start_at]+match+rest)
            
            self.combo_edit.showPopup()
            self.combo_edit.findChild(QListView).installEventFilter(self)
            #self.combo_edit.lineEdit().setFocus()
    
    # Input/output methods
    
    def add_output(self, output, format):
    
        if not output:
            return
        
        cursor = self.text_browser.textCursor()
        cursor.movePosition(QTextCursor.End)
        cursor.setCharFormat(format)
        
        lines = output.split(u"\n")
        for line in lines[:-1]:
            cursor.insertText(line)
            if len(lines) > 1:
                cursor.insertBlock(self.block_format, format)
        
        cursor.insertText(lines[-1])
        
        cursor.movePosition(QTextCursor.End)
        self.text_browser.ensureCursorVisible()
    
    def read_stdin(self, buf, data):
    
        new_data, ok = QInputDialog.getText(
            appname, self.tr("Request for input"), QLineEdit.Normal,
            data, self
            )
        
        if ok:
        
            # Echo stdin on stdout.
            self.add_output(new_data, self.input_format)
            self.add_output("\n", self.input_format)
            
            return str(new_data)+"\n"
        
        else:
        
            return str(data)
    
    def wrap_text(self, data):
    
        # Reformat, taking carriage return characters into account.
        lines = string.split(data, u"\n")

        new_lines = []

        for line in lines:

            # Split the line at each carriage return encountered.
            words = string.split(line, u"\r")

            # Construct a new line by overlaying all the "words"
            # which make up the line.
            length = reduce(lambda x,y: max(x, len(y)), words, 0)
            new_line = u" " * min(self.display_width, length)

            for word in words:

                # Simply overlay the word over the current line string.
                # This is not optimal if the words are very long; in that
                # case they should be wrapped at a specified column number.
                new_line = word + new_line[len(word):]

            new_lines.append(new_line)

        data = string.join(new_lines, u"\n")

        return data
    
    def write_value(self, text):
    
        output = self.wrap_text(text)
        
        self.add_output(output, self.output_format)
    
    def encode_input(self, element, text = u""):
    
        text = text.replace(u"&", u"&amp;")
        text = text.replace(u"<", u"&lt;")
        text = text.replace(u">", u"&gt;")
        if text:
            return u"<%s>%s</%s>\n" % (element, text, element)
        else:
            return u"<%s />" % element


def main():

    # Find the path to this program.
    global app_path, app_file
    
    app_path, app_file = os.path.split(sys.argv[0])
    
    # If the working directory is not in the sys.path list then add it.
    
    pwd = os.getenv("PWD")
    
    if pwd != "" and pwd not in sys.path:
    
        sys.path.insert(0, pwd)
    
    app = QApplication(sys.argv)
    
    # The GUI interface object checks whether the command line has finished
    # and, if so, exits the application.
    gui = GUIInterface(app)
    gui.resize(640, 480)
    gui.show()
    
    # Wait for the Qt application to exit.
    return app.exec_()


if __name__ == "__main__":

    result = main()
    
    # Exit
    sys.exit(result)
