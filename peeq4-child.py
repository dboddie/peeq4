#!/usr/bin/env python

"""
peeq4-child.py - Provides a client process to execute Python code.

Copyright (C) 2006-2010 David Boddie 

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
MA  02110-1301, USA.
"""

import imp, os, sys, traceback
from PyQt4.QtCore import QEvent, QObject, QString, QThread, SIGNAL
from PyQt4.QtGui import QApplication, qApp, QTextBrowser
from PyQt4.QtXml import QDomDocument

from PEEQ4.child_extensions import Extensions
from PEEQ4.handlers import StreamReader
from PEEQ4.iobuffer import Buffer

# Import the __future__ module to enable code using future language changes
# to be compiled interactively.

import __future__


# Define the application details.

appname = "peeq4-client"
__version__ = "0.10"


import __builtin__, keyword


class MatchError(Exception):

    pass

class Completer:

    extra_attributes = ("__class__", "__base__", "__module__", "__bases__")
    
    def __init__(self, module):
    
        self.module = module
        self.match_try = 0
        self.maximum_tries = 2
    
    def find_object(self, text, delimiters = "(, "):
    
        # Find the maximum index to start at. If no starting indices are found
        # then -1 is used. This is fine because we actually want to start
        # scanning the text at the beginning of the string in that situation.
        indices = map(
            lambda delimiter: text.rfind(delimiter), delimiters
            )
        
        start_at = max(indices) + 1
        return start_at, text[start_at:]
    
    def descend_name(self, pieces, namespaces):
    
        if pieces == []:
        
            return None
        
        piece = pieces.pop(0).strip()
        
        if pieces == []:
        
            matches = []
            
            for namespace in namespaces:
            
                matches = matches + filter(
                    lambda name: name.find(piece) == 0, dir(namespace)
                    )
            
            if matches == []:
            
                matches = matches + filter(
                    lambda name: name.find(piece) == 0, keyword.kwlist
                    )
            
            return matches
        
        else:
        
            matches = []
            
            for namespace in namespaces:
            
                if piece in dir(namespace):
                
                    obj = getattr(namespace, piece)
                    
                    matches = matches + map(
                        lambda name: piece+"."+name,
                        self.descend_name(pieces, (obj,))
                        )
            
            return matches
    
    def extend_attributes(self, obj):
    
        dict = {}
        for attribute in self.extra_attributes:
        
            if hasattr(obj, attribute):
                dict.update({attribute: getattr(obj, attribute)})
        
        return dict
    
    def match(self, text, cursor):
    
        # Work backwards from the cursor looking for the start of object names.
        start_at, full_name = self.find_object(text[:cursor])
        
        namespaces = (self.module, __builtin__)
        
        try:
        
            return start_at, self.descend_name(full_name.split("."), namespaces)
        
        except MatchError:
        
            return len(text), []


class NoValueClass:

    pass

NoValue = NoValueClass()


class Traceback:

    def __init__(self, text):
    
        self.text = text


# Define an object to contain the license for this application.
class License:

    def __init__(self, text):
    
        self.name = text[1:].split(" ")[0]
        lines = text.lstrip().split("\n")
        lines[0] = "<h2>PEEQ4</h2>"
        lines[2] = "<h3>%s</h3>" % lines[2]
        lines.insert(4, "<pre>")
        lines.append("</pre>")
        self.text = "\n".join(lines)
    
    def __repr__(self):
    
        self.browser = QTextBrowser()
        #self.browser.setTextFormat(QTextBrowser.RichText)
        self.browser.append(self.text)
        self.browser.show()
        return ""

__license__ = License(__doc__)

# Define a docstring for the user module.
user_docstring = __doc__[1:__doc__.find("\n", 1)]


# Define a function to obtain the class attributes for an instance.

adir_function = """
def adir(obj):

    if hasattr(obj, "__class__") and not hasattr(obj, "__bases__"):
    
        return obj.__class__.__dict__.keys()
    
    else:
    
        return []
"""


class Tools:

    def format_exc(self):
    
        lines = map(
            lambda line: line,
            #traceback.format_exception_only(*(sys.exc_info()[:2]))
            traceback.format_exception(*(sys.exc_info()))
            )
        
        return Traceback("\n".join(lines))


class Environment(Tools):

    def __init__(self):
    
        # Import some modules to customise the environment.
        self.setup_environment()
        
        # Delayed code for execution.
        self.delayed = []
    
    def setup_environment(self):
    
        # Construct a module to use to contain Python executed by the
        # user.
        self.module = imp.new_module("__user__")
        
        # Add a docstring for this module.
        self.module.__doc__ = user_docstring
        
        # Copy the sys module into the user's environment.
        self.module.sys = sys
        
        # Define some functions for the user.
        
        # Introduce a function to return the class attributes of instance
        # objects.
        
        adir = compile(adir_function, "<setup_functions>", "exec")
        exec adir in self.module.__dict__
        
        # Set up an object containing qpython license information in the
        # user module.
        self.module.peeq4_license = __license__
        #self.module.LICENSE = __doc__[1:]
    
    def execute(self, command_line, use_delayed = 1):
    
        if len(command_line) == 0:
        
            return NoValue
        
        if use_delayed:
        
            if command_line[0] in " \t":
            
                # Add the line to the list of delayed command lines.
                self.delayed.append(command_line)
                return NoValue
            
            if self.delayed != []:
            
                command_lines = "".join(self.delayed) + command_line
            
            else:
            
                command_lines = command_line
        
        try:
        
            value = eval(command_lines, self.module.__dict__)
            
            # Set the _ variable to the value returned.
            self.module._ = value
            
            if use_delayed:
            
                # The pending input was executed successfully so clear
                # the delayed command lines list.
                self.delayed = []
            
            return value
        
        except SyntaxError:
        
            # Catch Syntax Errors and pass them on for another attempt
            # to compile and execute them.
            pass
        
        except:
        
            # Catch all other errors and report them.
            
            if use_delayed:
            
                # Clear the delayed command line list.
                self.delayed = []
            
            return self.format_exc()
        
        
        # Try again with the exec command.
        
        # Construct a flags word for any features imported from the
        # __future__ module. Peculiarly, this has to be done because
        # the compile function appears not to be aware of any __future__
        # imports done from the command line.
        
        flags = 0
        user_values = filter(
            lambda x: isinstance(x, __future__._Feature),
            self.module.__dict__.values()
            )
        
        for value in __future__.__dict__.values():
        
            if isinstance(value, __future__._Feature):
            
                if value in user_values:
                
                    flags = flags | value.compiler_flag
        
        try:
        
            # Compile the code using the flags determined from the imported
            # features. The last parameter indicates that these flags should
            # be taken into account when compiling.
            
            code = compile(command_lines, "__user__", "exec", flags, 0)
            exec code in self.module.__dict__
            
            if use_delayed:
            
                # The pending input was executed successfully so clear
                # the delayed command lines list.
                self.delayed = []
            
            # No values are returned from the execution of code in this
            # manner unless we are using existing lines from the delayed
            # lines list.
            return NoValue
        
        except SyntaxError:
        
            msg = sys.exc_info()[1].msg
            
            if msg.find("unexpected EOF while parsing") != -1:
            
                if use_delayed:
                
                    # Delay code for later execution.
                    self.delayed.append(command_line)
                
                return NoValue
            
            elif msg.find("expected an indented block") != -1:

                # Only take this seriously if there is no preceding text
                # or if these last two lines have the same indentation.
                
                if use_delayed and self.delayed != [] and \
                    ( self.indentation(self.delayed[-1]) != \
                      self.indentation(command_line)          ):
                
                    # Keep the line for later if there is preceding text and
                    # the indentation has changed.
                    
                    self.delayed.append(command_line)
                    return NoValue
            
            # An actual Syntax Error.
            
            if use_delayed:
            
                # Clear the delayed command line list.
                self.delayed = []
            
            return self.format_exc()
        
        except:
        
            # Other errors are caught here.
            
            if use_delayed:
            
                # Clear the delayed command line list.
                self.delayed = []
            
            return self.format_exc()
    
    def indentation(self, s, tabsize = 4):
    
        l = 0
        
        for i in s:
        
            if i == " ":
                l = l + 1
            elif i == "\t":
                l = l + tabsize
            else:
                break
        
        return l


class GUIInterface(QObject, Tools):

    """GUIInterface(QObject)
    
    This class handles the interactions between a CommandLine object and an
    Environment object.
    
    gui = GUIInterface(finished_event, input_event, environment)
    
    The GUIInterface object is a subclass of QObject in order to allow
    executed Python code to access Qt classes.
    """
    
    def __init__(self, environment):
    
        QObject.__init__(self)
        
        # Record the environment object.
        self.environment = environment
        self.environment.execute("from PyQt4.QtCore import *\n"
                                 "from PyQt4.QtGui import *\n")
        
        # Create a completer for the environment.
        self.completer = Completer(self.environment.module)
    
    def customEvent(self, event):
    
        if isinstance(event, CommandLineEvent):
        
            self.processInput(event.text)
            event.accept()
        
        elif isinstance(event, CompleteEvent):
        
            index, matches = self.completer.match(event.text, event.cursor)
            QApplication.postEvent(self.caller, MatchesEvent(index, matches))
            event.accept()
        
        elif isinstance(event, ExitEvent):
        
            self.exitSession()
            event.accept()
    
    def processInput(self, input_string):
    
        """processInput(self)
        
        Processes the input from the command line.
        
        Submit the user's input to the environment for compilation and
        execution, then display the result in the console.
        """
        
        # Examine the text submitted.
        
        # Submit it to the user's running environment.
        
        # Retrieve a string value from the environment's execute method.
        value = self.environment.execute(input_string)
        
        if isinstance(value, Traceback):
            self.send_error("output", value.text)
        elif value is None or value is NoValue:
            self.send_output("output")
        else:
            try:
                value = repr(value) + u"\n"
                self.send_output("output", value)
            except:
                value = self.format_exc()
                self.send_error("output", value.text)
        
        self.send_status()
    
    def exitSession(self):
    
        """exitSession(self)
        
        Shuts down the interpreter session in the Environment object.
        
        If the user asks the interpreter to exit, the CommandLine object
        will a emit a signal that causes this slot to be called.
        Therefore, we stop monitoring events and quit the application.
        """
        
        qApp.quit()
    
    def send_status(self):
    
        QApplication.postEvent(
            self.caller, ResponseEvent(self.environment.delayed != [])
            )
        #qApp.processEvents()
    
    def send_output(self, element, text = None):
    
        QApplication.postEvent(self.caller, OutputEvent(element, text))
    
    def send_error(self, element, text = None):
    
        QApplication.postEvent(self.caller, ErrorEvent(element, text))


class CommandLine(QThread):

    """CommandLine(QThread)
    
    This class provides a command line for reading user input.
    
    command_line = CommandLine(environment, receiver, finished_event,
                               input_event)
    
    The environment must be an Environment object which will perform the
    compilation and execution of Python source code.
    
    The receiver is the GUI handling object which communicates with Qt's
    event loop.
    
    The exit, finished and input events should be threading.Event objects;
    these must also be passed in a consistent manner to a GUIInterface
    object.
    """
    
    def __init__(self, receiver):
    
        QThread.__init__(self)
        
        self.receiver = receiver
        self.receiver.caller = self
        
        self.delayed = False
        self.waiting = False
        self.processing = True
        self.current_input = []
        self.stdout = sys.__stdout__
        sys.__stdout__ = sys.stdout = Buffer()
        sys.__stdout__.write_callback = self.process_output
        self.stderr = sys.__stderr__
        sys.__stderr__ = sys.stderr = Buffer()
        sys.__stderr__.write_callback = self.process_error
        
        self.complete_text = u""
        self.complete_cursor = 0
        self.stdin_parser = StreamReader(
            {"input": self.execute, "exit": self.end_session,
             "complete": self.complete, "text": self.set_complete_text,
             "cursor": self.set_complete_cursor}
            )
        self.parsing_stdin = False
    
    def complete(self, text):
    
        QApplication.postEvent(self.receiver,
            CompleteEvent(self.complete_text, self.complete_cursor))
    
    def execute(self, input_string):
    
        """stop = execute(self, input_type, input_string)
        
        Submit the input string to the Python execution environment for later
        execution, set the input event then wait for it to be cleared by the
        environment.
        """
        
        # Post an event to the hidden object so that it can execute the code.
        self.waiting = True
        
        QApplication.postEvent(self.receiver, CommandLineEvent(input_string))
        
    def end_session(self, text):
    
        print "Exiting (%s)...\n" % text
        self.waiting = False
        self.processing = False
        sys.stdin.close()
    
    def set_complete_cursor(self, text):
    
        try:
            self.complete_cursor = int(text)
        except ValueError:
            self.complete_cursor = 0
    
    def set_complete_text(self, text):
    
        self.complete_text = text
    
    def run(self, intro=None):
    
        """run(self, intro=None)
        
        Repeatedly accept XML input, parse and dispatch to the GUI thread.
        
        Don't interfere with the input string if the user input is terminated
        with a CTRL-D; raise an EOFError, if necessary, and catch the
        exception outside the processing loop.
        """
        
        self.stdout.write("<session>")
        self.stdout.flush()
        self.stderr.write("<session>")
        self.stderr.flush()
        
        try:
        
            while self.processing:
            
                # Wait until any previous input has been processed.
                while self.waiting and self.processing:
                    pass
                
                new_data = QString.fromUtf8(sys.stdin.readline())
                self.stdin_parser.source.setData(new_data)
                if not self.parsing_stdin:
                    self.stdin_parser.parse()
                    self.parsing_stdin = True
                else:
                    self.stdin_parser.parseContinue()
        
        except (KeyboardInterrupt, EOFError):
        
            pass
        
        self.stdout.write("</session>")
        self.stdout.flush()
        self.stderr.write("</session>")
        self.stderr.flush()
        
        QApplication.postEvent(self.receiver, ExitEvent())
    
    def customEvent(self, event):
    
        if isinstance(event, ResponseEvent):
        
            if event.delayed:
                self.send_output("incomplete")
            #else:
            #    self.stdout.write("<ready />\n")
            
            self.waiting = False
            event.accept()
        
        elif isinstance(event, MatchesEvent):
        
            matches = map(lambda match: self.prepare_output("match", match),
                          event.matches)
            index = "<index>%i</index>" % event.index
            self.stdout.write("<matches>"+index+"".join(matches)+"</matches>")
            self.stdout.flush()
            event.accept()
        
        elif isinstance(event, OutputEvent):
        
            self.send_output(event.element, event.text)
            event.accept()
        
        elif isinstance(event, ErrorEvent):
        
            self.send_error(event.element, event.text)
            event.accept()
    
    def process_output(self, buf, data):
    
        self.send_output("output", data)
    
    def process_error(self, buf, data):
    
        self.send_error("output", data)
    
    def prepare_output(self, element_name, text = None):
    
        dom = QDomDocument()
        element = dom.createElement(element_name)
        dom.appendChild(element)
        if text:
            text_node = dom.createTextNode(text)
            element.appendChild(text_node)
        
        return unicode(dom.toString())
    
    def send_output(self, element_name, text = None):
    
        self.stdout.write(self.prepare_output(element_name, text))
        self.stdout.flush()
    
    def send_error(self, element_name, text = None):
    
        self.stderr.write(self.prepare_output(element_name, text))
        self.stderr.flush()


next_event = QEvent.User
class CommandLineEvent(QEvent):

    def __init__(self, text):
    
        QEvent.__init__(self, QEvent.Type(next_event))
        self.text = text


next_event += 1
class ResponseEvent(QEvent):

    def __init__(self, delayed):
    
        QEvent.__init__(self, QEvent.Type(next_event))
        self.delayed = delayed


next_event += 1
class ExitEvent(QEvent):

    def __init__(self):
    
        QEvent.__init__(self, QEvent.Type(next_event))


next_event += 1
class OutputEvent(QEvent):

    def __init__(self, element, text):
    
        QEvent.__init__(self, QEvent.Type(next_event))
        self.element = element
        self.text = text


next_event += 1
class ErrorEvent(QEvent):

    def __init__(self, element, text):
    
        QEvent.__init__(self, QEvent.Type(next_event))
        self.element = element
        self.text = text


next_event += 1
class ExtensionEvent(QEvent):

    def __init__(self, direction, element, text):
    
        QEvent.__init__(self, QEvent.Type(next_event))
        self.direction = direction
        self.element = element
        self.text = text


next_event += 1
class CompleteEvent(QEvent):

    def __init__(self, text, cursor):
    
        QEvent.__init__(self, QEvent.Type(next_event))
        self.text = text
        self.cursor = cursor


next_event += 1
class MatchesEvent(QEvent):

    def __init__(self, index, matches):
    
        QEvent.__init__(self, QEvent.Type(next_event))
        self.index = index
        self.matches = matches


def main():

    # Find the path to this program.
    global app_path, app_file
    
    app_path, app_file = os.path.split(sys.argv[0])
    
    # If the working directory is not in the sys.path list then add it.
    
    pwd = os.getenv("PWD")
    
    if pwd != "" and pwd not in sys.path:
    
        sys.path.insert(0, pwd)
    
    app = QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(False)
    
    # Create an environment to run Python scripts in.
    environment = Environment()
    
    # The GUI interface object checks whether the command line has finished
    # and, if so, exits the application.
    hidden = GUIInterface(environment)
    
    # Create a thread to handle command line input and start it.
    command_line = CommandLine(hidden)
    
    command_line.start()
    
    # Wait for the Qt application to exit.
    result = app.exec_()
    
    # Ask the command line thread to stop.
    if not command_line.isFinished():
    
        #print "<output>Stopping the command line thread...</output>"
        #self.stdout.flush()
        command_line.waiting = False
        command_line.processing = False
        sys.stdin.close()
        command_line.wait()
        #print "<output>Exiting...</output>"
        sys.stdout.flush()
    
    return result


if __name__ == "__main__":

    result = main()
    
    # Exit
    sys.exit(result)
