#!/usr/bin/env

"""
iobuffer.py - provides buffer objects that can be used to intercept sys.stdout
              and sys.stderr

Copyright (C) 2004-2010, David Boddie

This file is part of PEEQ4.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""

# Use the StringIO function to contain a buffer for the encoded content.

try:

    from cStringIO import StringIO

except ImportError:

    from StringIO import StringIO


class Buffer:

    def __init__(self, *args):
    
        self.StringIO = StringIO(*args)
        self.read_callback = self.null_read_callback
        self.write_callback = self.null_write_callback
    
    def null_read_callback(self, buf, data):
    
        return data
    
    def null_write_callback(self, buf, data, flush = 0):
    
        pass
    
    def write(self, data):
    
        # Use another method to actually write the data without triggering
        # a callback.
        self._write(data)
        
        # Call callback.
        self.write_callback(self, data)
    
    def _write(self, data):
    
        self.StringIO.write(data)
    
    def read(self, *args):
    
        # Use another method to actually read the data without triggering
        # a callback.
        data = self._read(*args)
        
        # Call callback with the data in the buffer, giving the callback
        # the chance to modify it before its use.
        data = self.read_callback(self, data)
        
        return data
    
    def _read(self, *args):
    
        return self.StringIO.read(*args)
    
    def clear(self):
    
        self.StringIO.seek(0, 0)
        self.StringIO.truncate()
    
    def flush(self):
    
        self.write_callback(self, "", flush = 1)
        self.StringIO.flush()
#    
#    def isatty(self):
#    
#        self.StringIO.isatty()
#    
#    def getvalue(self, *args):
#    
#        self.StringIO.getvalue(*args)
#    
    def readline(self):
    
        # Use another method to actually read the data without triggering
        # a callback.
        data = self._readline()
        
        # Call callback with the data in the buffer, giving the callback
        # the chance to modify it before its use.
        data = self.read_callback(self, data)
        
        return data
    
    def _readline(self):
    
        return self.StringIO.readline()
    
    def readlines(self):
    
        # Use another method to actually read the data without triggering
        # a callback.
        lines = self._readlines()
        
        new_lines = []
        for line in lines:
        
            # Call callback.
            new_line = self.read_callback(self, data[:-1]) + "\n"
            new_lines.append(new_line)
        
        return new_lines
    
    def _readlines(self):
    
        return self.StringIO.readlines()
    
    def seek(self, *args):
    
        self.StringIO.seek(*args)
    
    def tell(self):
    
        return self.StringIO.tell()
    
    def writelines(self, *args):
    
        # Use another method to actually write the data without triggering
        # a callback.
        self._writelines(*args)
        
        # Call callback.
        self.write_callback(self, "\n".join(args))
    
    def _writelines(self, *args):
    
        self.StringIO.writelines(*args)
    
    def truncate(self, *args):
    
        if args != ():
        
            self.StringIO.seek(args[0])
        
        self.StringIO.truncate()


class History:

    def __init__(self):
    
        self.history = []
        self.position = 0
        self.current_text = ""
    
    def append(self, text):
    
        self.history.append(text)
        self.position = len(self.history)
    
    def previous(self, text):
    
        if self.position == len(self.history):
        
            self.current_text = text
        
        if len(self.history) == 0:
        
            return self.current_text
        
        self.position = max(0, self.position - 1)
        
        return self.history[self.position]
    
    def next(self, text):
    
        self.position = min(self.position + 1, len(self.history))
        
        if self.position == len(self.history):
            return self.current_text
        else:
            return self.history[self.position]
    
    def items(self):
    
        return self.position, self.history
    
    def select(self, index, text):
    
        self.current_text = text
        self.position = index
        
        return self.history[index]
