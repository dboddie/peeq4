"""
formatters.py - output formatting and HTML encoding objects

Copyright (C) 2004-2010, David Boddie

This file is part of PEEQ4.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""

# We will use the methods of minidom.Text objects to encode text for
# displaying as HTML content.
from xml.dom import minidom

# Use the StringIO function to contain a buffer for the encoded content.

try:

    from cStringIO import StringIO

except ImportError:

    from StringIO import StringIO

# Define a function to encapsulate the conversion of strings into an HTML
# compliant encoding.

whitespace = u"\t\n\r "

def encode(text, preformatted = True):

    # Encode the data for use as HTML content.
    t = minidom.Text()
    t.nodeValue = text
    s = StringIO()
    t.writexml(s)
    s.seek(0)
    htmltext = s.read()
    s.close()
    
    # Convert spaces to hard spaces to prevent formatting issues. These
    # occur even within preformatted elements, unfortunately.
    new = []
    
    if preformatted:
    
        for c in htmltext:
        
            if c == u" ":
                new.append(u"&nbsp;")
            elif c == u"\n":
                new.append(u"<br />")
            elif ord(c) < 32 and (c not in whitespace):
                new.append(u"\\x%02x" % ord(c))
            else:
                new.append(c)
    
    else:
    
        # Use a flag to distinguish between leading whitespace on each line
        # and other occurrences of whitespace in the string.
        leading = 1
        
        for c in htmltext:
        
            if c == u"\n":
            
                new.append(u"<br />")
                
                # Reset the leading flag.
                leading = 1
            
            elif ord(c) < 32 and (c not in whitespace):
            
                # Unprintable ASCII character
                new.append(u"\\x%02x" % ord(c))
                
                # Clear the leading flag.
                leading = 0
            
            elif ord(c) == 32 and leading == 1:
            
                # Leading space
                new.append(u"&nbsp;")
            
            else:
            
                new.append(c)
                
                # Clear the leading flag.
                leading = 0
    
    return u"".join(new)
