#!/usr/bin/env python

"""
editors.py - a Python editing environment for PEEQ4

Copyright (C) 2004-2010 David Boddie

This file is part of PEEQ4.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""

from PyQt4.QtCore import QRegExp, Qt, SIGNAL, SLOT
from PyQt4.QtGui import QAction, QBrush, QColor, QComboBox, QFileDialog, \
    QFont, QFontDatabase, QHBoxLayout, QKeySequence, QLabel, QPushButton, \
    QSpinBox, QSyntaxHighlighter, QTextCharFormat, QTextCursor, \
    QTextDocument, QTextEdit, QVBoxLayout, QWidget

from PEEQ4.constants import NoValue, Traceback
from PEEQ4.components import SettingsBar


class Editor(QWidget):

    def __init__(self, char_format):
    
        QWidget.__init__(self)
        
        self.script_path = u""
        
        self.text_edit = QTextEdit(self)
        self.highlighter = PythonHighlighter(self.text_edit.document(),
                                             char_format)
        
        self.run_button = QPushButton(self.tr("Run"))
        self.load_button = QPushButton(self.tr("Load"))
        self.save_button = QPushButton(self.tr("Save"))
        self.close_button = QPushButton(self.tr("Close"))
        
        fontLabel = QLabel(self.tr("Font:"))
        fontFamilyCombo = QComboBox()
        fontFamilyCombo.addItems(QFontDatabase().families())
        fontSizeSpinBox = QSpinBox()
        fontSizeSpinBox.setMinimum(8)
        fontSizeSpinBox.setMaximum(128)
        
        self.connect(self.run_button, SIGNAL("clicked()"), self.run_script)
        self.connect(self.load_button, SIGNAL("clicked()"), self.load_script)
        self.connect(self.save_button, SIGNAL("clicked()"), self.save_script)
        self.connect(self.close_button, SIGNAL("clicked()"), self, SLOT("close()"))
        self.connect(fontSizeSpinBox, SIGNAL("valueChanged(int)"), self.setTextSize)
        
        fontSizeSpinBox.setValue(12)
        
        panel_layout = QHBoxLayout()
        panel_layout.addStretch(1)
        panel_layout.addWidget(self.run_button)
        panel_layout.addWidget(self.load_button)
        panel_layout.addWidget(self.save_button)
        panel_layout.addWidget(self.close_button)
        
        layout = QVBoxLayout(self)
        layout.setMargin(0)
        layout.addWidget(self.text_edit, 1)
        layout.addLayout(panel_layout)
    
    def run_script(self):
    
        # Examine the text submitted.

        # Submit it to the user's running environment.
        
        # Retrieve a string value from the environment's execute method.
        text = self.text_edit.toPlainText()
        
        if text.endsWith("\n"):
        
            text.append("\n")
        
        self.emit(SIGNAL("executeCode(const QString &, bool)"), text, False)
    
    def load_script(self):
    
        path = QFileDialog.getOpenFileName(self, u"Load Script",
            self.script_path, u"*.py")
        
        if path.isNull():
            return
        
        try:
            text = open(unicode(path), "r").read()
            self.text_edit.setPlainText(text)
        
        except IOError:
            QMessageBox.warning(self, u"Load Script",
                u"Failed to load script.", QMessageBox.Cancel,
                QMessageBox.NoButton)
        else:
            self.script_path = unicode(path)
    
    def save_script(self):
    
        path = QFileDialog.getSaveFileName(self, u"Save Script",
            self.script_path, u"*.py")
        
        if path.isNull():
            return
        
        try:
            text = self.text_edit.toPlainText()
            open(unicode(path), "w").write(unicode(text))
        
        except IOError:
            QMessageBox.warning(self, u"Save Script",
                u"Failed to save script.", QMessageBox.Cancel,
                QMessageBox.NoButton)
        else:
            self.script_path = unicode(path)
    
    def setTextSize(self, size):
    
        cursor = QTextCursor(self.text_edit.document())
        cursor.setPosition(0)
        cursor.movePosition(QTextCursor.End, QTextCursor.KeepAnchor)
        format = cursor.charFormat()
        font = format.font()
        
        # Set the font size for the whole document.
        font.setPointSize(size)
        format.setFont(font)
        cursor.setCharFormat(format)
        
        # Set the default font for the editor.
        self.text_edit.document().setDefaultFont(font)


class PythonHighlighter(QSyntaxHighlighter):

    def __init__(self, document, base_format):
    
        QSyntaxHighlighter.__init__(self, document)
        
        self.base_format = base_format
        
        self.rules = []
        
        keywords = (
            "and",       "del",       "for",       "is",        "raise",
            "assert",    "elif",      "from",      "lambda",    "return",
            "break",     "else",      "global",    "not",       "try",
            "class",     "except",    "if",        "or",        "while",
            "continue",  "exec",      "import",    "pass",      "yield",
            "def",       "finally",   "in",        "print"
            )
        
        self.keywordFormat = QTextCharFormat(base_format)
        self.keywordFormat.setForeground(Qt.darkBlue)
        self.keywordFormat.setFontWeight(QFont.Bold)
        self.rules += map(lambda s: (QRegExp(r"\b"+s+r"\b"),
                          self.keywordFormat), keywords)
        
        self.singleLineCommentFormat = QTextCharFormat(base_format)
        self.singleLineCommentFormat.setForeground(Qt.darkGreen)
        self.rules.append((QRegExp(r"#[^\n]*"), self.singleLineCommentFormat))
        
        self.multiLineStringFormat = QTextCharFormat(base_format)
        self.multiLineStringFormat.setBackground(QBrush(QColor(127,127,255)))
        self.multiLineStringBegin = QRegExp(r'\"\"\"')
        self.multiLineStringEnd = QRegExp(r'\"\"\"')
        
        self.quotationFormat1 = QTextCharFormat(base_format)
        self.quotationFormat1.setForeground(Qt.blue)
        self.rules.append((QRegExp(r'\"[^\n]*\"'), self.quotationFormat1))
        
        self.quotationFormat2 = QTextCharFormat(base_format)
        self.quotationFormat2.setForeground(Qt.blue)
        self.rules.append((QRegExp(r"'[^\n]*'"), self.quotationFormat2))
    
    def highlightBlock(self, text):
    
        self.setCurrentBlockState(0)
        self.setFormat(0, len(text), self.base_format)
        
        startIndex = 0
        if self.previousBlockState() != 1:
            startIndex = self.multiLineStringBegin.indexIn(text)
        
        if startIndex > -1:
            self.highlightRules(text, 0, startIndex)
        else:
            self.highlightRules(text, 0, len(text))
        
        while startIndex >= 0:
        
            endIndex = self.multiLineStringEnd.indexIn(text,
                startIndex + len(self.multiLineStringBegin.pattern()))
            if endIndex == -1:
                self.setCurrentBlockState(1)
                commentLength = text.length() - startIndex
            else:
                commentLength = endIndex - startIndex + \
                                self.multiLineStringEnd.matchedLength()
                self.highlightRules(text, endIndex, len(text))
            
            self.setFormat(startIndex, commentLength, self.multiLineStringFormat)
            startIndex = self.multiLineStringBegin.indexIn(text,
                                      startIndex + commentLength)
    
    def highlightRules(self, text, start, finish):
    
        for expression, format in self.rules:
        
            index = expression.indexIn(text, start)
            while index >= start and index < finish:
                length = expression.matchedLength()
                self.setFormat(index, min(length, finish - index), format)
                index = expression.indexIn(text, index + length)


def test():

    te = QTextEdit()
    h = PythonHighlighter(te.document())
    te.show()
    return h, te
