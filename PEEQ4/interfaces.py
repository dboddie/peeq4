#!/usr/bin/env python

"""
interfaces.py - user interface components for PEEQ4

Copyright (C) 2004-2010, David Boddie

This file is part of PEEQ4.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""

from PyQt4.QtCore import QEvent, QSize, QTimer, Qt, SIGNAL
from PyQt4.QtGui import QBrush, QColor, QImage, QPainter, QPixmap, QScrollArea, \
                        QSizePolicy, QVBoxLayout, QWidget

thumbnail_width = 50
thumbnail_height = 50

class WidgetContainer(QWidget):

    def __init__(self, parent = None):
    
        QWidget.__init__(self, parent)
        
        self.widgets = {}
        
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
    
    def eventFilter(self, obj, event):
    
        if obj in self.widgets.keys():
        
            if event.type() == QEvent.Close and \
                obj.testAttribute(Qt.WA_DeleteOnClose):
            
                self.remove_widget(obj)
            
            elif event.type() == QEvent.ChildAdded or \
                event.type() == QEvent.ChildRemoved or \
                event.type() == QEvent.Resize or \
                event.type() == QEvent.Show:
            
                self.widgets[obj].update_picture()
        
        return 0
    
    def resize(self):
    
        QWidget.resize(self, thumbnail_width + 2*self.layout.margin(),
            len(self.widgets) * (thumbnail_height + self.layout.spacing()) + \
            2*self.layout.margin() - self.layout.spacing())
    
    def addWidget(self, widget):
    
        thumbnail = CapturedWidget(self, widget)
        self.widgets[widget] = thumbnail
        self.layout.addWidget(thumbnail)
        
        widget.installEventFilter(self)
        self.connect(widget, SIGNAL("destroyed()"), self.remove_widget)
        self.connect(thumbnail, SIGNAL("deleted"), self.remove_widget)
        
        self.resize()
    
    def remove_widget(self, widget = None):
    
        if widget is None:
        
            widget = self.sender()
        
        widget.removeEventFilter(self)
        
        if self.widgets.has_key(widget):
        
            thumbnail = self.widgets[widget]
            self.layout.removeWidget(widget)
            thumbnail.close()
            del self.widgets[widget]
        
        self.resize()


class WidgetBox(QScrollArea):

    def __init__(self, parent = None):
    
        QScrollArea.__init__(self, parent)
        self.layout_manager = WidgetContainer()
        self.setWidget(self.layout_manager)
    
    def add_widgets(self, widgets):
    
        for widget in widgets:
        
            self.layout_manager.addWidget(widget)


class CapturedWidget(QWidget):

    def __init__(self, parent, widget):
    
        QWidget.__init__(self, parent)
        
        self.brush = QBrush(QColor(0,0,0,0))
        self.widget = widget
        
        self.update_picture()
        QTimer.singleShot(5000, self.update_picture)
    
    def update_picture(self):
    
        try:
        
            # Create a picture of the widget's contents.
            pixmap = QPixmap.grabWidget(self.widget)
        
        except RuntimeError:
        
            self.emit(SIGNAL("deleted"), self.widget)
            self.close(1)
            return
        
        # Scale the pixmap to a default size.
        self.pixmap = pixmap.scaled(
            thumbnail_width, thumbnail_height, Qt.KeepAspectRatio,
            Qt.SmoothTransformation
            )
        
        self.update()
    
    def sizeHint(self):
    
        return QSize(thumbnail_width, thumbnail_height)
    
    def minimumSizeHint(self):
    
        return QSize(thumbnail_width, thumbnail_height)
    
    def paintEvent(self, event):
    
        painter = QPainter()
        painter.begin(self)
        painter.fillRect(0, 0, thumbnail_width, thumbnail_height, self.brush)
        painter.drawPixmap(
            thumbnail_width/2.0 - self.pixmap.width()/2.0,
            thumbnail_height/2.0 - self.pixmap.height()/2.0, self.pixmap
            )
        painter.end()
    
    def mousePressEvent(self, event):
    
        try:
        
            self.widget.show()
            self.widget.raise_()
        
        except RuntimeError:
        
            self.emit(SIGNAL("deleted"), self.widget)
            self.close(1)
