#!/usr/bin/env python

"""
handlers.py - XML handlers for parsing communication between processes.

Copyright (C) 2004-2010 David Boddie

This file is part of PEEQ4.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""

from PyQt4.QtCore import QObject
from PyQt4.QtXml import QXmlDefaultHandler, QXmlInputSource, QXmlSimpleReader


class StreamHandler(QXmlDefaultHandler):

    def __init__(self, handlers, error_handler = None):
    
        QXmlDefaultHandler.__init__(self)
        
        self.current_element = []
        self.current_handler = []
        self.current_data = []
        self.error_string = u""
        
        self.handlers = handlers
        self.stored_data = {}
        for handler in self.handlers.keys():
            self.stored_data[handler] = []
        
        self.error_handler = error_handler
    
    def startDocument(self):
    
        return True
    
    def startElement(self, namespaceURI, localName, qName, attributes):
    
        name = unicode(qName)
        if name in self.handlers:
        
            self.stored_data[name] = []
            self.current_handler.append(self.handlers[name])
            self.current_data.append(self.stored_data[name])
        
        self.current_element.append(name)
        return True
    
    def endElement(self, namespaceURI, localName, qName):
    
        if self.current_element:
        
            name = unicode(qName)
            
            if name != self.current_element[-1]:
                self.error_string = u"Mismatched elements"
                return False
            
            else:
            
                if name in self.handlers:
                
                    text = u"".join(self.stored_data[name])
                    self.current_handler[-1](text)
                    self.current_data[-1] = []
                    self.current_handler.pop()
                    self.current_data.pop()
            
            if name == u"exit":
                self.endDocument()
        
        self.current_element.pop()
        return True
    
    def characters(self, text):
    
        if self.current_data:
            self.current_data[-1].append(unicode(text))
        
        return True
    
    def fatalError(self, exception):
    
        if self.error_handler:
            self.error_handler(unicode(exception.message()))
        return False
    
    def ignorableWhitespace(self, text):
    
        if self.current_data:
            self.current_data[-1].append(unicode(text))
        
        return True
    
    def endDocument(self):
    
        return True


class StreamReader(QXmlSimpleReader):

    """Creates a stream reader that uses a collection of handlers to
    respond to input from the given device.
    
    The specified handlers and additional error handler are used to
    create a StreamHandler instance that performs the actual work of
    handling input.
    """
    
    def __init__(self, handlers, device = None, error_handler = None):
    
        QXmlSimpleReader.__init__(self)
        
        self.handler = StreamHandler(handlers, error_handler)
        self.setContentHandler(self.handler)
        self.setErrorHandler(self.handler)
        if device:
            self.source = QXmlInputSource(device)
        else:
            self.source = QXmlInputSource()
    
    def parse(self):
    
        return QXmlSimpleReader.parse(self, self.source, True)
    
    def parseContinue(self):
    
        return QXmlSimpleReader.parseContinue(self)
