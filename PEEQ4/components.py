#!/usr/bin/env python

"""
components.py - useful components for PEEQ4

Copyright (C) 2004-2010 David Boddie

This file is part of PEEQ4.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""

from PyQt4.QtCore import Qt, QPointF, SIGNAL, SLOT
from PyQt4.QtGui import QAction, QColor, QHBoxLayout, QIcon, QPainter, \
                        QPixmap, QPolygonF, QPushButton, QVBoxLayout, QWidget


class SettingsBar(QHBoxLayout):

    def __init__(self, parent = None):
    
        QHBoxLayout.__init__(self, parent)
        
        self.setupPixmaps()
        self.handleWidget = QPushButton()
        self.handleWidget.setIcon(self.closeIcon)
        self.connect(self.handleWidget, SIGNAL("clicked()"), self.toggleLayout)
        self.addWidget(self.handleWidget, 0)
        self.open = True
    
    def setupPixmaps(self):
    
        triangle = QPolygonF([QPointF(-3.75, -8), QPointF(3.75, 0), QPointF(-3.75, 8)])
        
        openPixmap = QPixmap(8, 32)
        openPixmap.fill(QColor(0, 0, 0, 0))
        painter = QPainter()
        painter.begin(openPixmap)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setBrush(QColor(0, 0, 0, 255))
        painter.translate(4, 16)
        painter.drawPolygon(triangle)
        painter.end()
        
        self.openIcon = QIcon(openPixmap)
        
        closePixmap = QPixmap(8, 32)
        closePixmap.fill(QColor(0, 0, 0, 0))
        painter = QPainter()
        painter.begin(closePixmap)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setBrush(QColor(0, 0, 0, 255))
        painter.translate(4, 16)
        painter.rotate(180)
        painter.drawPolygon(triangle)
        painter.end()
        
        self.closeIcon = QIcon(closePixmap)
    
    def toggleLayout(self):
    
        if self.open:
            for i in range(self.count()):
                child = self.itemAt(i).widget()
                if child and child != self.handleWidget:
                    child.hide()
            self.open = False
            self.handleWidget.setIcon(self.openIcon)
        else:
            for i in range(self.count()):
                child = self.itemAt(i).widget()
                if child and child != self.handleWidget:
                    child.show()
            self.open = True
            self.handleWidget.setIcon(self.closeIcon)
