#!/usr/bin/env python

"""
scriptwindow.py - An example showing how to use the child classes to execute
                  code within the same process as the main application.

Copyright (C) 2010 David Boddie 

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
MA  02110-1301, USA.
"""

import sys
from PyQt4.QtGui import *
peeq4_child = __import__("peeq4-child")


class ScriptWindow(QWidget):

    def __init__(self, parent = None):
    
        QWidget.__init__(self, parent)
        
        self.environment = peeq4_child.Environment()
        self.gui = peeq4_child.GUIInterface(self.environment)
        self.gui.caller = self
        
        layout = QVBoxLayout(self)
        self.browser = QPlainTextEdit()
        self.browser.setReadOnly(True)
        self.editor = QLineEdit()
        
        self.editor.returnPressed.connect(self.inputText)
        
        layout.addWidget(self.browser)
        layout.addWidget(self.editor)
    
    def inputText(self):
    
        self.browser.appendPlainText(u">>> " + self.editor.text())
        self.gui.processInput(unicode(self.editor.text()))
        self.editor.clear()
    
    def customEvent(self, event):
    
        if isinstance(event, peeq4_child.ResponseEvent):
        
            if event.delayed:
                pass
            
            self.waiting = False
            event.accept()
        
        elif isinstance(event, peeq4_child.MatchesEvent):
        
            # Do something with the list of matches in event.matches.
            # Do something with the index into the matches in event.index.
            event.accept()
        
        elif isinstance(event, peeq4_child.OutputEvent):
        
            if event.text is not None:
                self.browser.appendPlainText(event.text)
            event.accept()
        
        elif isinstance(event, peeq4_child.ErrorEvent):
        
            self.browser.appendPlainText(event.text)
            event.accept()


if __name__ == "__main__":

    app = QApplication(sys.argv)
    w = ScriptWindow()
    w.show()
    sys.exit(app.exec_())
